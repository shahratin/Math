var count = 1
for (problem, answer) in testProblemList {
    let syntax = Parser(expression: problem).node ?? Node(val: 0)
    let result = evaluate(node: syntax)
    print("\(count)) \(problem) = \(result) [\(result == answer ? "Correct" : String(answer))]")
    print(syntax)
    count += 1
}
