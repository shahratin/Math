import Foundation

func preprocessor(expression: String) -> String {
    return expression.replacingOccurrences(of: "log", with: "l")
}
