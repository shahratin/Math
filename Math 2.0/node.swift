import Foundation

class Node: NSObject, NSCopying {
    var mathOperator: MathOperator?
    var val: Double?
    var left: Node?
    var right: Node?
    init(mathOperator: MathOperator, left: Node?, right: Node?) {
        self.mathOperator = mathOperator
        self.left = left
        self.right = right
    }

    init(val: Double) {
        self.val = val
    }

    private static func staticCopy(node: Node?) -> Node? {
        let node = node
        if node == nil {
            return nil
        } else if node!.val != nil {
            return Node(val: node!.val!)
        } else {
            //let left = staticCopy(node: node!.left!)
            let l: Node?
            let r: Node?
            if let n = node!.left{
              l = staticCopy(node: n)
            }else{
                l = nil
            }
            if let n = node!.right{
              r = staticCopy(node: n)
            }else{
                r = nil
            }
            return Node(mathOperator: node!.mathOperator!, left: l, right: r)
        }
    }

    func copy(with _: NSZone?) -> Any {
        // To Do: Error Checking
        return Node.staticCopy(node: self)!
    }

    override var description: String {
        var ret = ""
        let horizontal = "╼ " // "─╼"
        //let vertical: String.Element = "#"
        let vertical: String.Element = "│"
        let verticalRight = "├"
        let bottomAngle = "└"
        func getDescription(node: Node, space: String) {
            if let v = node.val {
                //ret += "="
                ret += space
                ret += String(v)
                ret += "\n "
            }else if node.left != nil, node.right == nil{
                ret += space
                ret += String(node.mathOperator!.symbol)
                ret += "\n "
                if let l = node.left{
                    //fix here
                getDescription(node: l, space: space + "==>")
                }
            }else if node.left == nil, node.right != nil{
                ret += space
                ret += String(node.mathOperator!.symbol)
                ret += "\n "
                if let r = node.right{
                    //fix here
                getDescription(node: r, space: space + "==>")
                }
            } else {
                ret += space
                ret += String(node.mathOperator!.symbol)
                ret += "\n "
                if let l = node.left{
                getDescription(node: l, space: space + "==>")
                }
                if let r = node.right{
                getDescription(node: r, space: space + "==>")
                }
            }
        }
        getDescription(node: self, space: "")
        let numbersSet = CharacterSet(charactersIn: ".0123456789\n ")
        let textCharacterSet = CharacterSet(charactersIn: ret)
        let isNumberOnly = textCharacterSet.isSubset(of: numbersSet)
        
        if ret.firstIndex(of: "\n") == nil {
            return ret
        }
        
        if isNumberOnly{
            print(isNumberOnly)
            return ret
        }
        
        // If ret contains more than one line, post processing occurs
        var retArray = ret.components(separatedBy: "\n")
        var processedArray: [String] = [""]
        retArray.removeLast()
        var processedLine = ""
        for line in retArray {
            if let index = line.lastIndex(of: ">") {
                var indexBefore = line.index(before: index)
                let indexAfter = line.index(after: index)
                indexBefore = line.index(before: indexBefore)
                indexBefore = line.index(before: indexBefore)
                processedLine = line.replacingOccurrences(of: "=", with: " ")
                processedLine = processedLine.replacingOccurrences(of: ">", with: " ")
                let leftSegment = String(processedLine.prefix(upTo: indexBefore))
                let rightSegment = String(processedLine.suffix(from: indexAfter))
                processedLine = leftSegment + String(vertical) + horizontal + rightSegment
                processedArray.append(processedLine)
            } else {
                processedArray.append(line)
            }
        }
        var lineNo = 0
        var arrayDict: [Int: [Int]] = [0: [1, 1]]
        processedArray.removeFirst()
        for line in processedArray {
            if let index = line.firstIndex(of: vertical) {
                let distance = line.distance(from: line.startIndex, to: index)
                if arrayDict[distance] != nil {
                    arrayDict[distance]!.append(lineNo)
                } else {
                    arrayDict[distance] = [lineNo]
                }
            }
            lineNo += 1
        }
        for (distance, array) in arrayDict {
            var count = 0
            for _ in array {
                if count % 2 != 0 {
                    for index in array[count - 1] ... array[count] {
                        //problem digit only expression
                        var currentString = processedArray[index]
                        if distance > 0 {
                            let charIndex = currentString.index(currentString.startIndex, offsetBy: distance)
                            let indexAfter = currentString.index(after: charIndex)
                            let leftSegment = String(currentString.prefix(upTo: charIndex))
                            let symbol = String(currentString.suffix(from: charIndex).prefix(upTo: indexAfter))
                            let rightSegment = String(currentString.suffix(from: indexAfter))
                            //processedArray[index] = leftSegment + String(vertical) + rightSegment
                            if (rightSegment == ""){
                                //here was the problem: missing symbol
                                processedArray[index] = leftSegment + symbol
                            }else{
                                processedArray[index] = leftSegment + String(vertical) + rightSegment
                            }
                        } else {
                            currentString.removeFirst()
                            //it's ok
                            processedArray[index] = String(vertical) + currentString
                        }
                    }
                    // First line
                    var index = array[count - 1]
                    var currentString = processedArray[index]
                    if distance > 0 {
                        let charIndex = currentString.index(currentString.startIndex, offsetBy: distance)
                        let indexAfter = currentString.index(after: charIndex)
                        let leftSegment = String(currentString.prefix(upTo: charIndex))
                        let rightSegment = String(currentString.suffix(from: indexAfter))
                        processedArray[index] = leftSegment + String(verticalRight) + rightSegment
                    } else {
                        currentString.removeFirst()
                        processedArray[index] = String(verticalRight) + currentString
                    }
                    // Last line
                    index = array[count]
                    currentString = processedArray[index]
                    if distance > 0 {
                        let charIndex = currentString.index(currentString.startIndex, offsetBy: distance)
                        let indexAfter = currentString.index(after: charIndex)
                        let leftSegment = String(currentString.prefix(upTo: charIndex))
                        let rightSegment = String(currentString.suffix(from: indexAfter))
                        processedArray[index] = leftSegment + String(bottomAngle) + rightSegment
                    } else {
                        currentString.removeFirst()
                        processedArray[index] = String(bottomAngle) + currentString
                    }
                }
                count += 1
            }
        }
        return processedArray.joined(separator: "\n")
    }

}
