import Foundation

class MathOperator {
    var symbol: String.Element
    var operandPosition: OperandPosition
    var mathOperation: (Double, Double) -> Double
    init(symbol: String.Element, operand: OperandPosition, operation: @escaping (Double, Double) -> Double) {
        self.symbol = symbol
        self.operandPosition = operand
        self.mathOperation = operation
    }
    
    private static func factorial(_ number: Double) -> Double {
        var ret = Int(number)
        for cur in 1 ..< ret {
            //To Do: Error handler
            ret = cur * ret
        }
        return Double(ret)
    }

    public static var list: [MathOperator] = [
        // MathOperator(symbol: "", mathOperation: {(left: Double, right: Double) -> Double in return }),
        MathOperator(symbol: "+", operand: .leftAndRight, operation: {$0 + $1}),
        MathOperator(symbol: "-", operand: .leftAndRight, operation: {$0 - $1}),
        MathOperator(symbol: "×", operand: .leftAndRight, operation: {$0 * $1}),
        MathOperator(symbol: "÷", operand: .leftAndRight, operation: {$0 / $1}),
        MathOperator(symbol: "^", operand: .leftAndRight, operation: {pow($0, $1)}),
        MathOperator(symbol: "√", operand: .rightOnly, operation: {_,right in sqrt(right)}),
        MathOperator(symbol: "%", operand: .leftOnly, operation: {left,_ in left / 100}),
        MathOperator(symbol: "!", operand: .leftOnly, operation: {left,_ in factorial(left)}),
        MathOperator(symbol: "㏒", operand: .rightOnly, operation: {_,right in log(right)/log(10)}),
    ]
}

