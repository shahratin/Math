import Foundation

enum OperandPosition{
    case leftAndRight;
    case leftOnly;
    case rightOnly;
}

class Parser {
    var operatorList: [MathOperator] {get {return MathOperator.list}}
    var node: Node?
    
    init(expression: String) {
        node = generateSyntax(expression: expression)
    }

    private func hasMathSymbol(expression: String) -> Bool {
        var hasMathSymbol = false
        if !expression.isEmpty {
            for mathOperator in operatorList {
                if expression.lastIndex(of: mathOperator.symbol) != nil {
                    hasMathSymbol = true
                    break
                }
            }
        }
        return hasMathSymbol
    }
    
    private func hasParenthesis(expression: String) -> Bool{
        var hasParenthesis = false
        if expression.lastIndex(of: "(") != nil{
            hasParenthesis = true
        }
        return hasParenthesis
    }
    
    private func createNode(mathOperator: MathOperator, left: Node, right: Node) -> Node{
        if mathOperator.operandPosition == OperandPosition.rightOnly{
            return Node(mathOperator: mathOperator, left: nil, right: right)
        } else if mathOperator.operandPosition == OperandPosition.leftOnly{
            return Node(mathOperator: mathOperator, left: left, right: nil)
        } else {
            return Node(mathOperator: mathOperator, left: left, right: right)
        }
    }

    func generateSyntax(expression: String) -> Node? {
        var node: Node?

        if expression.isEmpty {
            return nil
        } else if hasParenthesis(expression: expression) {
            print("parenthesis present")
            return nil
        } else if !hasMathSymbol(expression: expression){
            if expression == "π"{
                return Node(val: Double.pi)
            } else {
                return Node(val: Double(expression) ?? 0.00)
            }
        } else {
            for mathOperator in operatorList {
                if let index = expression.lastIndex(of: mathOperator.symbol) {
                    let leftExpression = String(expression.prefix(upTo: index))
                    let afterIndex = expression.index(after: index)
                    let rightExpression = String(expression.suffix(from: afterIndex))

                    let leftHasMathSymbol = hasMathSymbol(expression: leftExpression)
                    let rightHasMathSymbol = hasMathSymbol(expression: rightExpression)

                    if leftHasMathSymbol, rightHasMathSymbol {
                        let left = generateSyntax(expression: leftExpression) ?? Node(val: 0)
                        let right = generateSyntax(expression: rightExpression) ?? Node(val: 0)
                        node = createNode(mathOperator: mathOperator, left: left, right: right)
                    } else if leftHasMathSymbol {
                        let left = generateSyntax(expression: leftExpression) ?? Node(val: 0)
                        var right: Node
                        if rightExpression == "π"{
                            right = Node(val: Double.pi)
                        }else{
                            right = Node(val: Double(rightExpression) ?? 0.00)
                        }
                        node = createNode(mathOperator: mathOperator, left: left, right: right)
                    } else if rightHasMathSymbol {
                        var left: Node
                        if leftExpression == "π"{
                            left = Node(val: Double.pi)
                        }else{
                            left = Node(val: Double(leftExpression) ?? 0.00)
                        }
                        let right = generateSyntax(expression: rightExpression) ?? Node(val: 0)
                        node = createNode(mathOperator: mathOperator, left: left, right: right)
                    } else {
                        var right: Node
                        if rightExpression == "π"{
                            right = Node(val: Double.pi)
                        }else{
                            right = Node(val: Double(rightExpression) ?? 0.00)
                        }
                        var left: Node
                        if leftExpression == "π"{
                            left = Node(val: Double.pi)
                        }else{
                            left = Node(val: Double(leftExpression) ?? 0.00)
                        }
                        node = createNode(mathOperator: mathOperator, left: left, right: right)
                    }
                    break
                }
            }
            return node
        }
    }
}

func evaluate(node: Node) -> Double {
    let rootNode = (node.copy() as? Node)!
    var currentNode = rootNode
    func eval() {
        if rootNode.val != nil {}
        if currentNode.left != nil,
            currentNode.right != nil,
            currentNode.left!.left == nil,
            currentNode.left!.right == nil,
            currentNode.right!.left == nil,
            currentNode.right!.right == nil {
            var ret = 0.00
            ret = currentNode.mathOperator?.mathOperation(currentNode.left!.val!, currentNode.right!.val!) ?? 0.00

            currentNode.left = nil
            currentNode.right = nil
            currentNode.val = ret
            currentNode = rootNode
            eval()
        }
        
        if currentNode.left != nil,
            currentNode.right == nil,
            currentNode.left!.left == nil,
            currentNode.left!.right == nil {
            var ret = 0.00
            ret = currentNode.mathOperator?.mathOperation(currentNode.left!.val!, 0.00) ?? 0.00

            currentNode.left = nil
            currentNode.right = nil
            currentNode.val = ret
            currentNode = rootNode
            eval()
        }
        
        if currentNode.left == nil,
            currentNode.right != nil,
            currentNode.right!.left == nil,
            currentNode.right!.right == nil {
            var ret = 0.00
            ret = currentNode.mathOperator?.mathOperation(0.00, currentNode.right!.val!) ?? 0.00

            currentNode.left = nil
            currentNode.right = nil
            currentNode.val = ret
            currentNode = rootNode
            eval()
        }
        if currentNode.left != nil, currentNode.right != nil {
            let temp = currentNode
            currentNode = currentNode.left!
            eval()
            currentNode = temp
            if currentNode.right != nil {
                currentNode = currentNode.right!
                eval()
            }
        }

        if currentNode.left != nil {
            if let r = currentNode.right{
            currentNode = r
            }
            eval()
        }
        if currentNode.right != nil {
            if let l = currentNode.left{
            currentNode = l
            }
            eval()
        }
    }
    eval()
    return rootNode.val ?? -1.00
}
